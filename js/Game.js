window.APP = window.APP || {};

(function(APP){
	'use strict';

	let
		CONFIG,
		STATE,
		CONST
	;

	APP.Game = class Game {
		constructor({grid_constructor, actor_constructor, food_constructor}){
			CONFIG = APP.CONFIG;
			CONST = APP.CONST;
			STATE = CONST.STATE;

			if(!grid_constructor || !actor_constructor || !food_constructor){
				throw new Error('You should pass grid, actor, food instances into constructor!');
			}
			this._state = STATE.READY;
			this._grid = new grid_constructor();
			this._actor_constructor = actor_constructor;
			this._food_constructor = food_constructor;
			this._speed = CONFIG.FPS;

			return this;
		}
		update(){

			if(this._direction_queue.length)
				this._direction = this._direction_queue.shift();

			this._actor.move(this._direction);

			if (this.checkCollide()) {
				this.end();
				return this;
			}

			this.checkEating(this._food).updateLevel();

			return this;
		}
		start(){
			//this.addControlsListenner();
			// set first body cell [25,25] in the center of the grid
			this.addActor().addFood();
			this._state = STATE.PLAYING;
			this._score = 0;
			this._speed = CONFIG.FPS;
			this._level = 0;
			this._direction = CONST.UP;
			this._direction_queue = [];
			return this;
		}
		end(){
			//this.removeControslListenner();
			this._state = STATE.GAMEOVER;
			return this;
		}
		addActor(){
			this._actor = new this._actor_constructor();
			this._actor.setPosition(this._grid.center);
			return this;
		}
		addFood(){
			let random_pos = this._grid.randomPosition;
			if (this._actor.isCellInBody(random_pos)){
				this.addFood();
				return;
			}
			this._food = new this._food_constructor(CONFIG.FOOD_VALUE);
			this._food.setPosition(random_pos);
			return this;
		}
		updateLevel(){
			let level = CONFIG.LEVELS.SLUG;
			if (this._actor.size >= CONFIG.LEVELS.WARM) {
				level = CONFIG.LEVELS.WARM;
			}
			if ( this._actor.size >= CONFIG.LEVELS.PYTHON ) {
				level = CONFIG.LEVELS.PYTHON;
			}
			this._level = level;
			return this;
		}
		checkCollide(){
			// return true if there is collide with body or border
			return this._actor.isBodyCollide() || this.checkBorderCollide();
		}
		checkEating(food){
			if (this._actor.head[0] === food.position[0] && this._actor.head[1] === food.position[1]){
				// actor eat
				this._actor.eat(food.calories);
				this._score += food.calories;
				this._speed = Math.min(++this._speed, CONFIG.MAX_FPS);
				this.addFood();
			}
			return this;
		}
		checkBorderCollide(){
			// return true
			// if row less then 0 or more then grid height
			// or if coll less then 0 or more then grid width
			let cell = this._actor.head;
			//console.log('board colide,', cell[0] < 0 || cell[0] > this._grid.height-1 || cell[1] < 0 || cell[1] > this._grid.width-1);
			return cell[0] < 0 || cell[0] > this._grid.height-1 || cell[1] < 0 || cell[1] > this._grid.width-1;
		}
		get grid(){
			return this._grid;
		}
		get actor(){
			return this._actor;
		}
		get food(){
			return this._food;
		}
		get state(){
			return this._state;
		}
		get speed(){
			return this._speed;
		}
		set state(state){
			this._state = state;
		}
		get score(){
			return this._score;
		}
		get level(){
			return this._level;
		}
		set direction(direction) {
			// add direction if directions are not oppositely directed
			if ( 	this._direction + direction !== 0 &&
					this._direction_queue[0] !== direction &&
					this._direction_queue[0] + direction !== 0
				)
				this._direction_queue.push(direction);
		}
	}

})(window.APP);