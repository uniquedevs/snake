window.APP = window.APP || {};

(function(APP){
	'use strict';

	let OFFSET,
		CONST,
		CONFIG
	;

	class Queue {
		// the queue is a body of the snake. It represented as Array of [row, col] coordinates of adjacent cells. Ex: [[0,0],[0,1],[0,2]] - is horizontal body with length 3.
		// It proceed with 'First In Last Out' method
		// Head of the Snake is element with index 0
		// Elements constantly are added to the head and removed from tail of the body during the snake movement
		constructor(items){

			this._items = Array.isArray(items) ? items : [];

			this.allButFirst = {
				// iterates over all elements exept first
				[Symbol.iterator]: function() {
					let index = 0,
						data = this._items
					;

					return {
						next: () => ({value: data[++index], done: !(index in data)})
					}
				}.bind(this)
			};

			return this;
		}
		toString(){
			let str = '';
			for(let cell = 0; cell < this._items.length; cell++){
				str += '(' + this._items[cell] + ')';
			}
			return str;
		}
		enqueue(item){
			// add first element
			this._items.unshift(item);
			return this;
		}
		dequeue(){
			// removed last element
			this._items.pop();
			return this;
		}
		get length(){
			return this._items.length;
		}
		clear(){
			this._items = [];
			return this;
		}
		get first(){
			// this is a head of the snake
			return this._items[0];
		}
	}

	APP.Snake = class Snake {
		constructor(){
			CONST = APP.CONST;
			CONFIG = APP.CONFIG;

			OFFSET = new Map([
				[CONST.UP,		[-1,0]],
				[CONST.DOWN, 	[1,0]],
				[CONST.LEFT, 	[0,-1]],
				[CONST.RIGHT, 	[0,1]]
			]);

			this._body = new Queue();
			this._body_growth = 5;
		}
		toString(){
			return this._body.toString();
		}
		move(direction){
			let new_head_position = this.getNewHeadPosition(direction)
			;
			this._body.enqueue(new_head_position);

			if (this._body_growth) {
				// If the snake grows
				this._body_growth--;
			} else {
				this._body.dequeue();
			}
			return this;
		}
		eat(calories){
			// 3 is a body grow factor
			this._body_growth = calories * CONFIG.BODY_GROW_FACTOR;
		}
		getNewHeadPosition(direction){
			let body_head = this._body.first
			;
			return [body_head[0] + OFFSET.get(direction)[0], body_head[1] + OFFSET.get(direction)[1]];
		}
		isBodyCollide(){
			let snake_head = this._body.first
			;

			return this.isCellInTail(snake_head);
		}
		isCellInTail(cell){
			let snake_tail = this._body.allButFirst
			;

			for (let snake_tail_cell of snake_tail) {
				if (cell[0] === snake_tail_cell[0] && cell[1] === snake_tail_cell[1])
					return true;
			}
			return false;
		}
		isCellInHead(cell){
			let snake_head = this._body.first
			;
			return cell[0] === snake_head[0] && cell[1] === snake_head[1];
		}
		isCellInBody(cell){
			return this.isCellInTail(cell) || this.isCellInHead(cell);
		}
		set body_growth(cells) {
			this._body_growth += cells;
		}
		get head(){
			return this._body.first;
		}
		get tail(){
			return this._body.allButFirst;
		}
		get size(){
			return this._body.length;
		}
		setPosition(position){
			this._body.enqueue(position);
		}
	}


})(window.APP);