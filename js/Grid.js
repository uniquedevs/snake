window.APP = window.APP || {};

(function (APP) {
	'use strict';

	let EMPTY;

	APP.Grid = class Grid {
		constructor() {
			const 	CONST = APP.CONST,
					CONFIG = APP.CONFIG
			;
			EMPTY = CONST.EMPTY;
			this._height = CONFIG.GRID_HEIGHT;
			this._width = CONFIG.GRID_WIDTH;

			// this method doesn't have practical sence for now.
			// It may be used for implementing the machine player
			this.clear();
		}

		toString() {
			let str = '\n';
			for (let row = 0; row < this._height; row++) {
				str += this._cells[row].toString();
				str += '\n';
			}
			return str;
		}

		clear() {
			// fill grid with empty cells.
			// grid is two dimensional array of cells,
			// where inner arrays represent coordinates [row, col]
			let inner = new Array(this._width).fill(EMPTY);

			this._cells = Array.from({
				length: this._height,
			}, () => inner.slice());
		}

		get width() {
			return this._width;
		}

		get height() {
			return this._height;
		}

		get randomPosition(){
			let random_x = Math.floor(Math.random() * this._width),
				random_y = Math.floor(Math.random() * this._height);
			return [random_x, random_y];
		}

		get center() {
			// math floor the center position of the grid
			return [this._width / 2 ^ 0, this._height / 2 ^ 0];
		}
	}

})(window.APP);