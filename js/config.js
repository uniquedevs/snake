window.APP = window.APP || {};

(function(APP){
	'use strict';

	const 	CONFIG = {
				GRID_WIDTH: 50,
				GRID_HEIGHT: 50,
				CELL_SIZE: 10,
				FPS: 5,
				MAX_FPS: 25,
				TITLE: 'SNAKE',
				BODY_GROW_FACTOR: 5,
				LEVELS: {
					SLUG: 0,
					WARM: 50,
					PYTHON: 100
				},
				FOOD_VALUE: 1
			},
			CONST = {
				EMPTY: 0,
				FULL: 1,
				UP: -2,// opposite direction must be zerro summ
				DOWN: 2,
				LEFT: -3,
				RIGHT: 3,
				STATE : {
					READY: 4,
					PAUSED: 5,
					PLAYING: 6,
					GAMEOVER: 7,
					STENDBY: 8
				}
			}
	;

	APP.CONFIG = Object.freeze(CONFIG);
	APP.CONST = Object.freeze(CONST);

})(window.APP);