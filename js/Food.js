window.APP = window.APP || {};

(function(APP){
	'use strict';

	APP.Food = class Food {
		constructor(calories){
			const CONST = APP.CONST;

			this._calories = calories;
			this._positon = [0,0];
			return this;
		}
		setPosition(position){
			this._positon = position;
			return this;
		}
		get calories(){
			return this._calories;
		}
		get position(){
			return this._positon;
		}
	}


})(window.APP);