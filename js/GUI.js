window.APP = window.APP || {};

(function(APP){
	'use strict';

	const 	INTERFACE = {
				BOARD_PADDING:[50, 10, 50, 10],
				POPUP_WIDTH: 200,
				POPUP_HEIGHT: 120,
				BTN_WIDTH: 180,
				BTN_HEIGHT: 40,
				TEXT_SIZE: 24
			},
			COLOR_SCHEME = {
				PRIMARY: '#3a3afc',
				SECONDARY: '#05FFD2',
				ACCENT: '#FCDB41',
				TEXT_LIGHT: '#FFFFFF',
				TEXT_DARK: '#000000'
			}
	;

	let
			CONFIG,
			CONST,
			STATE,
			KEYMAP,
			TOGGLER
	;

	APP.HELPER = class HELPER {
		static fillRect(ctx, rect){
			let {x,y,width,height,color} = rect
			;
			ctx.fillStyle = color;
			ctx.fillRect(x, y, width, height);
		}
		static strokeRect(ctx, rect){
			let {x,y,width,height,stroke_style} = rect;
			ctx.beginPath();
			ctx.strokeStyle = stroke_style;
			ctx.rect(x,y,width,height);
			ctx.stroke();
		}
		static fillCircle(ctx, circle){
			let {x,y,radius, color} = circle
			;
			ctx.fillStyle = color;
			//ctx.beginPath();
			ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
			ctx.fill();
		}
		static clearRect(ctx, rect){
			let {x,y,width,height} = rect
			;
			ctx.clearRect(x,y,width,height);
		}
		static fillText(ctx, text_area){
			let {x, y, fill_style, font, text_align, text} = text_area;
			ctx.fillStyle = fill_style;
			ctx.font = font;
			ctx.textAlign = text_align;
			ctx.fillText(text, x, y);
		}
		static getMousePosition(canvas, event){
			//Function to get the mouse position
			let rect = canvas.getBoundingClientRect();
			return {
				x: event.clientX - rect.left,
				y: event.clientY - rect.top
			};
		}
		static isInside(pos, rect){
			//Function to check whether a point is inside a rectangle
			return pos.x > rect.x && pos.x < rect.x+rect.width && pos.y < rect.y+rect.height && pos.y > rect.y;
		}
	};

	APP.GUI = class GUI {
		constructor({game}){

			CONFIG = APP.CONFIG;
			CONST = APP.CONST;
			STATE = CONST.STATE;

			KEYMAP = new Map(
					[
						[38,CONST.UP],
						[40,CONST.DOWN],
						[37,CONST.LEFT],
						[39,CONST.RIGHT]
					]
			);

			TOGGLER = new Map(
					[
						[STATE.PAUSED, STATE.PLAYING],
						[STATE.PLAYING, STATE.PAUSED]
					]
			);

			if(!game){
				throw new Error('You sould pass game instance into constructor!');
			}

			this._canvas = document.querySelector('canvas');
			this._canvas.setAttribute('tabindex','0');
			this._ctx = this._canvas.getContext("2d");

			this._game = game;
			this._cell_size = CONFIG.CELL_SIZE;

			//variables for managing time
			this._now = null;
			this._then = Date.now();
			this._interval = null;
			this._delta = null;

			this._board_rect = {
				// board is a grid where the game action proceeds
				x: INTERFACE.BOARD_PADDING[3],
				y: INTERFACE.BOARD_PADDING[0],
				width: game.grid.width * this._cell_size,
				height: game.grid.height * this._cell_size,
			};

			this._canvas_rect = {
				// width and height of canvas includes padding around the board
				x: 0,
				y: 0,
				width: INTERFACE.BOARD_PADDING[1] + this._board_rect.width + INTERFACE.BOARD_PADDING[3],
				height: INTERFACE.BOARD_PADDING[0] + this._board_rect.height + INTERFACE.BOARD_PADDING[2],
			};

			this._canvas.width = this._canvas_rect.width;
			this._canvas.height = this._canvas_rect.height;

			this._snake_line_width = CONFIG.CELL_SIZE;

			// callbacks invoked on interface events
			this._onStartBtnClick = this.onStartBtnClick.bind(this);
			this._onResumeBtnClick = this.onResumeBtnClick.bind(this);

			//stenby prevent drawing new frame if true
			this._stendby = false;
		}
		onStartBtnClick(){
			this._game.start();
		}
		onResumeBtnClick(){
			this._game.state = STATE.PLAYING;
		}
		onKeyDown(e){
			let key = e.keyCode;

			if (~[37,38,39,40].indexOf(key) && this._game.state === STATE.PLAYING) {
				// arrow keys change movement direction of the actor only when state is playing

				this._game.direction = KEYMAP.get(key);
				e.preventDefault();
			}

			if (key === 32 && TOGGLER.has(this._game.state)) {
				// space button toggle pause - play
				this._game.state = TOGGLER.get(this._game.state);
				e.preventDefault();
			}
		}
		addControlsListenner(){
			document.onkeydown = this.onKeyDown.bind(this);
			return this;
		}
		start(){
			this.addControlsListenner();
			this._canvas.focus();
			this.nextFrame();

			return this;
		}
		nextFrame() {
			requestAnimationFrame(this.nextFrame.bind(this));

			this._now = Date.now();
			this._delta = this._now - this._then;
			// this._game.speed is FPS value
			this._interval = 1000 / this._game.speed;

			if (this._delta > this._interval) {
				// update time staff
				this._then = this._now - (this._delta % this._interval);

				// code for drawing frame
				this.drawFrame();
			}

			return this;
		}
		drawFrame(){
			if (this._stendby && this._game.state !== STATE.PLAYING)
				return this;

			if (this._game.state === STATE.PLAYING) {
				this._game.update();
				// turn off stendby mode if the game is playing
				this._stendby = false;
			}

			switch (this._game.state){

				case STATE.READY:
					this.drawGameboard();
					this.showPopup('NEW GAME', {text:'START', callback: this._onStartBtnClick});
					break;
				case STATE.GAMEOVER:
					this.showPopup('GAME OVER', {text:'TRY AGAIN', callback: this._onStartBtnClick});
					break;
				case STATE.PAUSED:
					this.showPopup('PAUSED', {text:'RESUME', callback: this._onResumeBtnClick});
					break;
				case STATE.PLAYING:
					this.drawGameboard()
							.drawFood()
							.drawSnake()
							.drawProgress();
					break;
			}
			return this;
		}
		showPopup(text, btn){
			let btn_rect = this.drawPopup(text, btn.text),
				onCanvasClick = (e) => {
					let mousePos = APP.HELPER.getMousePosition(this._canvas, e);

					if (APP.HELPER.isInside(mousePos, btn_rect)) {
						btn.callback();
						this._canvas.removeEventListener('click', onCanvasClick);
					}
				}
			;
			this._canvas.addEventListener('click', onCanvasClick, false);

			// prevent draw frame circle when popup is opened
			this._stendby = true;

			return this;
		}
		drawGameboard(){
			let canvas_rect = Object.assign({color: COLOR_SCHEME.PRIMARY}, this._canvas_rect),
				board_rect = Object.assign({stroke_style: COLOR_SCHEME.TEXT_LIGHT, line_width: "1"}, this._board_rect)
			;

			APP.HELPER.clearRect(this._ctx, canvas_rect);

			// all game area
			APP.HELPER.fillRect(this._ctx, canvas_rect);

			// start drawing the game board
			APP.HELPER.strokeRect(this._ctx, board_rect);

			this.drawCaptions();

			return this;
		}
		drawSnake(){
			let snake_head = this._game.actor.head,
				snake_tail = this._game.actor.tail,
				snake_cell_size = this._cell_size - 2,
				head_pos = this.cellToPixelPosition(snake_head),
				snake_head_rect = {
					x: head_pos[0] - snake_cell_size / 2,
					y: head_pos[1] - snake_cell_size / 2,
					width: snake_cell_size,
					height: snake_cell_size,
					color: COLOR_SCHEME.TEXT_LIGHT
				}
			;

			// draw head
			APP.HELPER.fillRect(this._ctx, snake_head_rect);

			// draw tail
			for (let snake_tail_cell of snake_tail) {
				let teil_pos = this.cellToPixelPosition(snake_tail_cell),
					snake_cell_rect = {
						x: teil_pos[0] - snake_cell_size / 2,
						y: teil_pos[1] - snake_cell_size / 2,
						width: snake_cell_size,
						height: snake_cell_size,
						color: COLOR_SCHEME.SECONDARY
 					};

				APP.HELPER.fillRect(this._ctx, snake_cell_rect);
			}

			return this;
		}
		drawFood(){
			let food = this._game.food,
				food_pos = this.cellToPixelPosition(food.position),
				food_rect = {
					x: food_pos[0] - this._cell_size / 2,
					y: food_pos[1] - this._cell_size / 2,
					width: this._cell_size,
					height: this._cell_size,
					color: COLOR_SCHEME.ACCENT
				}
			;

			// draw food
			APP.HELPER.fillRect(this._ctx, food_rect);

			return this;
		}
		drawPopup(text, btn_text){
			// btn is an Object containing info about btn text and onclick callback function
			let canvas_width = this._canvas_rect.width,
				canvas_height = this._canvas_rect.height,
				popup_width = INTERFACE.POPUP_WIDTH,
				popup_height = INTERFACE.POPUP_HEIGHT,
				btn_width = INTERFACE.BTN_WIDTH,
				btn_height = INTERFACE.BTN_HEIGHT,
				text_size = INTERFACE.TEXT_SIZE,
				popup_rect = {
					x: canvas_width / 2 - popup_width / 2,
					y: canvas_height / 2 - popup_height / 2,
					width: popup_width,
					height: popup_height,
					color: COLOR_SCHEME.TEXT_LIGHT
				},
				btn_rect = {
					x: popup_rect.x + (popup_width - btn_width) / 2,
					y: popup_rect.y + popup_rect.height - btn_height*1.5,
					width: btn_width,
					height: btn_height,
					color: COLOR_SCHEME.SECONDARY
				}
			;
			//draw popup container
			APP.HELPER.fillRect(this._ctx, popup_rect);

			//draw poput text
			if (text) {
				let text_area = {
					x: canvas_width / 2,
					y: popup_rect.y + btn_rect.height,
					fill_style: COLOR_SCHEME.PRIMARY,
					font: text_size + 'px Barrio',
					text_align: 'center',
					text: text
				};
				APP.HELPER.fillText(this._ctx, text_area);
			}

			//draw button
			APP.HELPER.fillRect(this._ctx, btn_rect);
			if (btn_text) {
				let text_area = {
					x: canvas_width / 2,
					y: btn_rect.y + btn_rect.height - 12 ,
					fill_style: COLOR_SCHEME.TEXT_DARK,
					font: text_size + 'px Barrio',
					text_align: 'center',
					text: btn_text
				};
				APP.HELPER.fillText(this._ctx, text_area);
			}

			return btn_rect;
		}
		drawCaptions(){
			let bottom_captions_pos_y = this._canvas_rect.y + this._canvas_rect.height - (INTERFACE.BOARD_PADDING[2] - INTERFACE.TEXT_SIZE) / 2
			;
			let text_size = INTERFACE.TEXT_SIZE,
				title_area = {
					x: this._canvas_rect.width / 2,
					y: this._canvas_rect.y +  INTERFACE.BOARD_PADDING[0] / 2,
					fill_style: COLOR_SCHEME.TEXT_LIGHT,
					font: text_size + 'px Barrio',
					text_align: 'center',
					text: CONFIG.TITLE
				},
				score_area = {
					x: this._canvas_rect.x + this._canvas_rect.width * 0.2,
					y: bottom_captions_pos_y,
					fill_style: COLOR_SCHEME.TEXT_LIGHT,
					font: text_size + 'px Barrio',
					text_align: 'right',
					text: 'SCORE : '
				},
				level_area = {
					x: this._canvas_rect.x + this._canvas_rect.width * 0.8,
					y: bottom_captions_pos_y,
					fill_style: COLOR_SCHEME.TEXT_LIGHT,
					font: text_size + 'px Barrio',
					text_align: 'right',
					text: 'LEVEL : '
				};
			APP.HELPER.fillText(this._ctx, title_area);
			APP.HELPER.fillText(this._ctx, score_area);
			APP.HELPER.fillText(this._ctx, level_area);
		}
		drawProgress(){
			let LEVEL_MAP = new Map([
					[CONFIG.LEVELS.SLUG, 'SLUG'],
					[CONFIG.LEVELS.WARM, 'WARM'],
					[CONFIG.LEVELS.PYTHON, 'PYTHON']
				]),
				bottom_captions_pos_y = this._canvas_rect.y + this._canvas_rect.height - (INTERFACE.BOARD_PADDING[2] - INTERFACE.TEXT_SIZE) / 2,
				text_size = INTERFACE.TEXT_SIZE,
				score_area = {
					x: this._canvas_rect.x + this._canvas_rect.width * 0.2,
					y: bottom_captions_pos_y,
					fill_style: COLOR_SCHEME.TEXT_LIGHT,
					font: text_size + 'px Barrio',
					text_align: 'left',
					text: this._game.score
				},
				level_area = {
					x: this._canvas_rect.x + this._canvas_rect.width * 0.8,
					y: bottom_captions_pos_y,
					fill_style: COLOR_SCHEME.TEXT_LIGHT,
					font: text_size + 'px Barrio',
					text_align: 'left',
					text: LEVEL_MAP.get(this._game.level)
				};
			APP.HELPER.fillText(this._ctx, score_area);
			APP.HELPER.fillText(this._ctx, level_area);
		}
		cellToPixelPosition(cell){
			// get cell on the grid and return coordinates of the center of this cell in pixels within canvas
			let
				board_top_left_x = this._board_rect.x,
				board_top_left_y = 	this._board_rect.y,
				cell_position_x = cell[1] * this._cell_size ,
				cell_position_y = cell[0] * this._cell_size,
				half_cell = this._cell_size / 2,
				cell_to_pixel_center_x = board_top_left_x + cell_position_x + half_cell,
				cell_to_pixel_center_y = board_top_left_y + cell_position_y + half_cell
			;

			return [cell_to_pixel_center_x, cell_to_pixel_center_y];
		}
	}

})(window.APP);