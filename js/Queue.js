(function(APP){
	'use strict';

	const 	CONFIG = APP.CONFIG,
			CONST = APP.CONST
	;

	APP.Queue = class Queue {
		constructor(items){
			this._items = Array.isArray(items) ? items : [];

			this.allButFirst = {
				[Symbol.iterator]: function() {
					let index = 0,
							data = this._items;

					return {
						next: () => ({value: data[++index], done: !(index in data)})
					}
				}.bind(this)
			};
		}
		toString(){
			let str = '';
			for(let cell = 0; cell < this._items.length; cell++){
				str += '(' + this._items[cell] + ')';
			}
			return str;
		}
		enqueue(item){
			this._items.unshift(item);
		}
		dequeue(){
			this._items.pop();
		}
		clear(){
			this._items = [];
		}
		get first(){
			return this._items[0];
		}
	}

})(window.APP || {});