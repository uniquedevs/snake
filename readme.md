# SNAKE

### Gameplay
    The player controls a square on a bordered plane. 
    As it moves forward, it leaves a trail behind, resembling a moving snake. 
    The snake continually gets longer as it eats. 
    The player loses when the snake runs into the screen border or itself.

### How to start playing
    Start index.html in browser (Chrome, FF, Safary supported, IE not tested)
    JS files are included into the index.html without bundler
    No third party libraries
    
### Controls
    Press arrow keys to navigate the snake.
    Press space button to pause/resume the game